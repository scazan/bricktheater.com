import React from 'react';
import BrickMap from './img/BrickMap.png';
import BrickHeaderBack from './img/bricknyita.jpg';
import residencyPic from './img/residencypicture.jpg';
import styled from 'styled-components';

// const primaryColor = '#ff5959';
const primaryColor = '#070707';
// const secondaryColor = 'rgb(255, 45, 45)';
const secondaryColor = '#efefef';


const Main = styled.div`
  color: ${primaryColor};
  background: ${secondaryColor};

  font-family: Averia Serif Libre;
  font-family: Cantata One;
  font-family: Cardo;
  font-family: Cinzel;
  font-family: Judson;
  font-family: Noto sans, sans-serif;
  letter-spacing: -0.01em;

  a, a:hover, a:visited {
    color: ${primaryColor};
  }
`;

const Header = styled.header`

  font-family: Assistant, serif;
  border-bottom: 0.5vh solid ${primaryColor};

  font-size: 38vh;
  h1 {
    padding: 6vh;
    border-right: 0.5vh solid ${primaryColor};
    letter-spacing: -0.02em;
    text-transform: uppercase;
    background: #2700ff;

    &:hover {
      color: #fff;

        text-shadow: 1px 1px 1px #000,
        -1px -1px 1px #000,
        1px -1px 1px #000,
        -1px 1px 1px #000;
      background-image: url(${BrickHeaderBack});
      background-postion: 50% 50%;
      background-size: 100vw;

      animation: 20s scroll infinite linear;
    }
  }

`;

const Address = styled.div`
  display: grid;
  grid-template-columns: 60vw 40vw;
    border-top: 0.5vh solid ${primaryColor};

  h2 {

    font-size: 11vh;
    padding: 6vh;
    padding-top: 4vh;

    &:first-child {
      border-right: 0.5vh solid ${primaryColor};
    }


  &.mailingList {
    &:hover {
      background-image: url(${residencyPic});
      background-size: 60vw;
        animation: 20s scrollLeft infinite linear;

      color: #fff;
        text-shadow: 1px 1px 1px #000,
        -1px -1px 1px #000,
        1px -1px 1px #000,
        -1px 1px 1px #000;

        input {
          background: #fff;
        }
    }
  }
    &.map {

      &:hover {
        color: rgba(255,255,255);
        text-shadow: 1px 1px 1px #000,
        -1px -1px 1px #000,
        1px -1px 1px #000,
        -1px 1px 1px #000;
        background-image: url(${BrickMap});
        background-size: 40vw;
        background-position: 50% 50%;
        animation: 20s scrollRight infinite linear;
      }
    }

    label {
      font-size: 6vh;
      letter-spacing: -0.05em;
    }
  }
`;

const MailingListSignup = styled.input`
  display: block;
  width: calc(100% - 2vh);
  height: 6vh;
  margin-top: 3vh;

  background: transparent;
  border: 0.5vh solid #000;
  color: #000;

  &::placeholder {
    color: #000;
  }

  font-size: 3vh;
  font-family: Assistant, serif;

  padding: 1vh;
`;

const Section = styled.div`
  // background: rgb(29, 255, 130);
  display: grid;
  grid-template-columns: 40vw 60vw;
  &.alt {
    grid-template-columns: 60vw 40vw;
  }

  border-bottom: 0.5vh solid ${primaryColor};

  p {
    font-size: 3vh;
    line-height: 4vh;
    margin-bottom: 6vh;
  }

  h2 {
    font-family: Assistant, serif;
    font-size: 11vh;
    margin-bottom: 6vh;
  }
  div {
    padding: 6vh;
  }
  div:first-child {
    border-right: 0.5vh solid ${primaryColor};
  }
`;

const App: React.FC = () => {
  return (
    <Main>
      <Header>
        <h1>
          The Brick
        </h1>
        <Address>
          <h2 className="mailingList">
            <p>
              Coming in January 2020
            </p>
            <label>
              Sign up for our mailing list
            </label>
            <MailingListSignup placeholder="Type your email"></MailingListSignup>
          </h2>
          <a href="https://www.google.com/maps/place/579+Metropolitan+Ave,+Brooklyn,+NY+11211,+USA/@40.7142975,-73.951963,17z" target="_blank">
            <h2 className="map">
              579 Metropolitan Ave. 
              Brooklyn, NY
            </h2>
          </a>
        </Address>
      </Header>
      <Section>
        <div>
          <h2>Mission Statement</h2>
          <p>
            The Brick Theater, Inc is a not-for-profit theater company dedicated to nurturing the work of emerging artists at its performance space in Williamsburg, Brooklyn—The Brick.
          </p>
          <p>
            The Brick presents world premieres, monthly performance series, and seasonal festivals, expanding Williamsburg’s profile as a destination for cutting-edge art and entertainment.
          </p>
          <p>
            The Brick continues to seek new artists and projects, to provide them with a creative home, and to serve as Williamsburg’s primary incubator of innovative theater arts.
          </p>
        </div>

        <div>
          <h2>About Us</h2>
          <p>
            Winner of the The Caffe Cino Fellowship Award, The Brick is Williamsburg, Brooklyn’s destination for cutting-edge theatrical experience. The Brick and its company, The Brick Theater, Inc, were founded in 2002 by Robert Honeywell and Michael Gardner. Formerly an auto-body shop, a yoga center, and various storage spaces, this brick-walled garage in Williamsburg, Brooklyn, was completely refurbished as a state-of-the-art performance space.
          </p>
          <p>
            The Brick has been home to many critically acclaimed premieres, including Bouffon Glass Menajoree (NY IT Award Winner—Outstanding Play), In a Strange Room (Time Out New York’s Top Ten Plays), Samuel & Alasdair: A Personal History of the Robot War (NY IT Award Winner—Outstanding Play), Suspicious Package (ITBA Award Winner—Best Unique Theatrical Experience), Craven Monkey and the Mountain of Fury (NY IT Award Nominee—Outstanding Performance Art Production), and Greed: a Musical Love $tory (NY IT Award Nominee—Outstanding Musical), The Brick has hosted some of downtown theater’s most innovative artists, including Annie Baker, Young Jean Lee, The Debate Society, Thomas Bradshaw and Nick Jones.
          </p>
          <p>
            The Summer Theme Festival Series presented The Hell Festival in 2004, The Moral Values Festival in 2005, The $ellout Festival in 2006, The Pretentious Festival in 2007, The Film Festival: A Theater Festival in 2008, The Antidepressant Festival in 2009, The Too Soon Festival in 2010, The Comic Book Theater Festival in 2011 and Democracy this coming June of 2012. In addition, The Brick has also produced a short-works program called Brick-a-Brac, a collection of holiday-themed one-act plays known as The Baby Jesus One-Act Jubilee, the hugely successful New York Clown Theatre Festival (the first of its kind in New York in over twenty years), and the annual Game Play festival (which celebrates video games performance arts).
          </p>
        </div>
      </Section>
      <Section className="alt">
        <div>
          <h2>Rentals</h2>
          <p>
            Classes: $25/hour<br/>
            Auditions: $25/hour<br/>
            Rehearsals: $25/hour<br/>
            For inquiries, please contact The Brick at rentals@bricktheater.com. Please note, our minimum for rental consideration is 25 hours of rehearsal rental or 5 hours of performance rental.
          </p>
          <p>
            For comedy show proposals at The Brick, please contact Brooklyn Comedy Collective at info@brooklyncc.com.
          </p>
          <p>
            One-Time Events/Performances with Audience/Benefits/Film Shoots/Screenings/Other Event Rentals
            $85/hour. 
          </p>
          <p>
            Film and TV Studio Holding or Filming Rentals
            $1500/day. 
          </p>
          <p>
            For inquiries, please contact The Brick at rentals@bricktheater.com. Please note, our minimum for rental consideration is 25 hours of rehearsal rental for 6 hours of performance rental.

          </p>
          <p>
            To see pictures of The Brick in Repertory mode (most months of the year), click here.

          </p>
          
        </div>

        <div>
          <h2>Production Rentals</h2>
          <p>
            We only occasionally rent to productions outright. Most non-summer month programming are adjudicated under the Resident Artist model for exclusive week-long runs. Straight rentals are $3,000/week and must accommodate our 10pm late night series by performing at 7pm. For details, please visit www.bricktheater.com/applications or contact rentals at bricktheater dot com.

          </p>
          <p>
            The Brick presents world premieres, monthly performance series, and seasonal festivals, expanding Williamsburg’s profile as a destination for cutting-edge art and entertainment.
          </p>
          <p>
            The Brick continues to seek new artists and projects, to provide them with a creative home, and to serve as Williamsburg’s primary incubator of innovative theater arts.
          </p>
        </div>
      </Section>
    </Main>
  );
}

export default App;
